from shorty import app


if __name__ == "__main__":
    app.run()

# Test working in command line
# gunicorn --bind 127.0.0.1:5010 wsgi:app

# Deploy on unix sock
# gunicorn --workers 3 --bind unix:inporbes_link_shortener.sock -m 007 wsgi:app




 # /home/hosein/link_shortener/venv/bin/gunicorn --workers 3 --bind unix:/home/hosein/link_shortener/project/inporbes_link_shortener.sock wsgi:app

