# -*- coding: utf-8 -*-

import re

from flask_wtf import FlaskForm

from wtforms import TextField, SubmitField
from wtforms.widgets import TextInput
from wtforms.validators import Required, Optional, Length, URL, Regexp

from shorty.core.forms.widgets import XXLargeTextInput

RE_SLUG = re.compile(r'^[-\w]+$')


class URLForm(FlaskForm):
    url = TextField('URL', [Required(), Length(max=255), URL()], widget=XXLargeTextInput())
    slug = TextField('Slug', [Optional(), Length(max=30), Regexp(RE_SLUG)], widget=TextInput())
    submit = SubmitField('shorten')
