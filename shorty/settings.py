# -*- coding: utf-8 -*-
from local_settings import *

# BASE_URL = 'http://localhost:5000'
BASE_URL = "http://95.211.228.204"
MAIN = 'http://inprobes.com'
QR_IMAGES_PATH = '/static/qr/%s'
PREFIX = '_'

DEBUG = True
TESTING = False

SECRET_KEY = 'DuMmY sEcReT kEy'
CSRF_ENABLED = True
CSRF_SESSION_KEY = '_csrf_token'

# SQLALCHEMY_DATABASE_URI = 'sqlite:///shorty.sqlite'
SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/{}'.format(
    DB_USERNAME, DB_PASSWORD, DB_HOST, DB_NAME
)
SQLALCHEMY_TRACK_MODIFICATIONS = False

try:
    from .local_settings import *
except ImportError:
    pass
