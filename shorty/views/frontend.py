# -*- coding: utf-8 -*-
import pyqrcode
from flask import *
from flask.views import MethodView, View
from sqlalchemy.orm.exc import NoResultFound

from shorty.core.shortener import shorten_url, expand_url, EncoderError
from shorty.forms import URLForm
from shorty.models import ShortURL
from shorty.settings import QR_IMAGES_PATH

frontend = Blueprint('frontend', __name__)


class IndexView(MethodView):
    template = 'main.html'

    def get(self):
        form = URLForm()
        return render_template(self.template,
                               form=form)

    def post(self):
        form = URLForm()
        slug = request.json.get('slug') if request.json else form.data.get('slug')
        url = request.json.get('url') if request.json else form.data.get('url')

        # Check if slug exists, return error and skip followings
        if slug and ShortURL.query.filter_by(slug=slug).one_or_none():
            return jsonify({'error': 'Slug exists'}), 400

        # Check whether API is called or not
        if request.json and 'url' in request.json:
            short_code, new = shorten_url(url, slug)
            short_url = '%s/%s' % (current_app.config['BASE_URL'], short_code)
            response = jsonify({'url': short_url}), 200

        # Just form submitted
        else:
            if not form.validate_on_submit():
                return render_template(self.template, form=form)
            short_code, new = shorten_url(url, slug)
            short_url = '%s/%s' % (current_app.config['BASE_URL'], short_code)
            response = render_template(self.template, form=form,
                                       short_url=short_url)

        filename = short_code + '.png'
        path = QR_IMAGES_PATH % filename
        if new:
            full_path = 'shorty' + path
            number = pyqrcode.create(short_url)
            number.png(full_path, scale=10)

        if request.json:
            response = jsonify({'url': short_url, 'qr': path}), 201
        else:
            response = render_template(self.template, form=form,
                                       short_url=short_url, qr=path)
        return response


class ShortLinkRedirectView(View):
    methods = ['GET']

    def dispatch_request(self, short_code):
        try:
            long_url = expand_url(short_code)
            return redirect(long_url)
        except (EncoderError, NoResultFound):
            flash('Unknown short URL', category='error')
            return redirect(url_for('frontend.index'))
        else:
            return redirect(long_url)
